// import TRANSLATION_DATA from "./i18n/data/index.json" assert { type: "JSON" };
import i18nEngine from "./i18n/internationalize.js";
//import SlideShow from "./components/slideshow.js";
import AnchorUpdator from "./components/anchorupdator.js";

const main = async () => {
  window.addEventListener("load", async () => {
    //new i18nEngine(indexTranslations).init();
    //new i18nEngine(navBarTranslations).init();
    new i18nEngine(await i18nEngine.getTranslationFile("index.json")).init()
    new i18nEngine(await i18nEngine.getTranslationFile("navbar.json")).init()
    AnchorUpdator();
  });
  /* Too janky */
  //new SlideShow(
  //  "../img/slideshow/slideshow",
  //  17,
  //  (total: number, path: string) =>
  //    Array.from(
  //      { length: total },
  //      (_: any, index: number) => `url("${path} (${index + 1}).png")`
  //    )
  //).start();
};

new Promise(() => {
  main();
});
