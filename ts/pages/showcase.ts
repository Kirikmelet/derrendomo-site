import i18nEngine from "../i18n/internationalize.js";
import {
  ContentSwitcher,
  ContentItem,
  ContentSwitcherMeta,
} from "../components/contentswitcher.js";
import AnchorUpdator from "../components/anchorupdator.js";
//import fontSwitcher from "../components/fontswitcher.js"
//import showcaseTranslations from "../../translation_data/pages/showcase.json";
//import southManorTranslations from "../../translation_data/pages/showcase/south_manor.json";
//import centralStationTranslations from "../../translation_data/pages/showcase/central_station.json";

const main = async () => {
  window.addEventListener("load", async () => {
    new i18nEngine(await i18nEngine.getTranslationFile("pages/showcase.json")).init();
    AnchorUpdator();
  });

  // Content Switcher
  const urlOrigin: string = import.meta.url;
  const contentList: Array<ContentItem> = [
    {
      image: new URL("../../img/showcase_imgs/bukit_manor_exterior.png", urlOrigin),
      id: "SouthManor",
      translationEngine: new i18nEngine(await i18nEngine.getTranslationFile("pages/showcase/south_manor.json")),
    },
    {
      image: new URL(
        "../../img/showcase_imgs/central_station_exterior.png",
        urlOrigin
      ),
      id: "CentralStation",
      translationEngine: new i18nEngine(await i18nEngine.getTranslationFile("pages/showcase/central_station.json")),
    },
  ];
  const switcher = new ContentSwitcher(
    {
      bgDivRootId: "SlideShowContent",
      contentBodyRootId: "ContentBody",
      bgCSSVar: "--bg",
    } as ContentSwitcherMeta,
    contentList
  );
  // Check if index number and set if so
  const paramIndex = new URLSearchParams(location.search).get("searchIndex");
  if (paramIndex !== "null" && paramIndex) {
    const newIndex = Number(paramIndex) ?? 0;
    switcher.setIndex(Number.isNaN(newIndex) ? 0 : newIndex);
  }
  switcher.update();
  const contentButtonNext = document.getElementById("SlideShowNext")!;
  const contentButtonPrev = document.getElementById("SlideShowPrev")!;
  contentButtonNext.addEventListener("click", () => {
    switcher.shiftIndex(1);
    switcher.update();
  });
  contentButtonPrev.addEventListener("click", () => {
    switcher.shiftIndex(-1);
    switcher.update();
  });
};

new Promise(() => {
  main();
});
