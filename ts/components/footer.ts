import i18nEngine from "../i18n/internationalize.js";
// import footerStyle from "bundle-text:/css/footer.css";
//import sealImg from "url:/img/seal.png";
//import gitlabLogo from "url:/img/gitlab-logo-600.png";
//import githubLogo from "url:/img/github-mark.png";
//import gmailLogo from "url:/img/Gmail_Logo_512px.png";
//import footTrans from "../../translation_data/footer.json";

export default class FooterElement extends HTMLElement {
  rootDiv: HTMLElement = document.createElement("footer");
  cssURL: URL = new URL("../../css/footer.css", import.meta.url);
  cssDiv: HTMLLinkElement = document.createElement("link");
  styleDiv: HTMLStyleElement = document.createElement("style");
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.rootDiv.appendChild(this.getLeftDiv());
    this.rootDiv.appendChild(this.getCenterDiv());
    this.rootDiv.appendChild(this.getRightDiv());
    this.cssDiv.rel = "stylesheet";
    this.cssDiv.href = this.cssURL.toString();
    // this.styleDiv.textContent = footerStyle;
    this.shadowRoot?.append(this.cssDiv);
    this.shadowRoot?.append(this.rootDiv);
    this.ensureTranslations();
  }
  static init() {
    if (!window.customElements.get("footer-el")) {
      window.customElements.define("footer-el", FooterElement);
    }
  }
  getLeftDiv() {
    let leftRoot = document.createElement("div");
    let nameHeader = document.createElement("h4");
    let logoImg = document.createElement("img");
    let description = document.createElement("p");
    nameHeader.setAttribute("data-i18n-key", "footer_name");
    nameHeader.innerText = "Negara Derrenomo";
    logoImg.loading = "lazy";
    logoImg.src = new URL("../../img/seal.png", import.meta.url).toString();
    //logoImg.src = sealImg
    logoImg.alt = "Derrenomo Seal";
    description.setAttribute("data-i18n-key", "footer_site_desc");
    description.innerText =
      "Official government site for the State of Derrenomo and its constutients";
    leftRoot.appendChild(nameHeader);
    leftRoot.appendChild(logoImg);
    leftRoot.appendChild(description);
    return leftRoot;
  }
  getCenterDiv() {
    let centerRoot = document.createElement("div");
    let name = document.createElement("h4");
    let description = document.createElement("p");
    let repoLink = FooterElement.urlParagraphGenerator(
      "GitLab",
      new URL("https://gitlab.com/Kirikmelet/derrendomo-site"),
      new URL("../../img/gitlab-logo-600.png", import.meta.url),
      "GitLab Logo"
    );
    name.setAttribute("data-i18n-key", "footer_foss_name");
    name.innerText = "Open-Source Repository";
    description.setAttribute("data-i18n-key", "footer_foss_desc");
    description.innerText =
      "This website is proudly licensed under the WTFPL 2.0 License";
    centerRoot.appendChild(name);
    centerRoot.appendChild(description);
    centerRoot.appendChild(repoLink);
    return centerRoot;
  }
  getRightDiv() {
    let rightRoot = document.createElement("div");
    let name = document.createElement("h4");
    let personalName = document.createElement("p");
    name.setAttribute("data-i18n-key", "footer_created_by");
    name.innerText = "Created By";
    personalName.innerText = "Troy Judho Dwijanto";
    rightRoot.appendChild(name);
    rightRoot.appendChild(personalName);
    rightRoot.appendChild(
      FooterElement.urlParagraphGenerator(
        "Kirikmelet",
        new URL("https://gitlab.com/Kirikmelet"),
        new URL("../../img/gitlab-logo-600.png", import.meta.url),
        "Gitlab logo"
      )
    );
    rightRoot.appendChild(
      FooterElement.urlParagraphGenerator(
        "Kirikmelet",
        new URL("https://github.com/Kirikmelet"),
        new URL("../../img/github-mark.png", import.meta.url),
        "Github logo"
      )
    );
    rightRoot.appendChild(
      FooterElement.urlParagraphGenerator(
        "troy.dwijanto2@gmail.com",
        new URL("mailto:troy.dwijanto2@gmail.com"),
        new URL("../../img/Gmail_Logo_512px.png", import.meta.url),
        "Gmaillogo"
      )
    );
    return rightRoot;
  }
  ensureTranslations() {
    window.addEventListener("load", async () => {
      new i18nEngine(
        await i18nEngine.getTranslationFile("footer.json"),
        this.rootDiv
      ).init();
    });
  }
  static urlParagraphGenerator(
    innerText: string,
    href: URL,
    imgSrc: URL,
    alt: string = "A generic image"
  ) {
    let inlineParagraph = document.createElement("p");
    let img = document.createElement("img");
    let anchor = document.createElement("a");
    inlineParagraph.id = "InlineURL";
    img.loading = "lazy";
    img.src = imgSrc.toString();
    img.alt = alt;
    anchor.href = href.toString();
    anchor.innerText = innerText;
    inlineParagraph.appendChild(img);
    inlineParagraph.appendChild(anchor);
    return inlineParagraph;
  }
}
