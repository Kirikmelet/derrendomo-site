// index.js
import i18nEngine from "../i18n/internationalize.js";
import anchorupdator from "./anchorupdator.js";
//import headerStyle from "bundle-text:/css/navbar.css"
//import navTrans from "../../translation_data/navbar.json"
//import showcaseURL from "url:/pages/showcase.html"
//import aboutUsURL from "url:/pages/aboutus.html"
//import contactURL from "url:/pages/contact.html"

export default class NavBar extends HTMLElement {
  urlOrigin = document.location.origin;
  rootDiv: HTMLDivElement;
  cssDiv: HTMLLinkElement = document.createElement("link") as HTMLLinkElement;
  cssURL: URL = new URL("../../css/navbar.css", import.meta.url);
  styleDiv: HTMLStyleElement = document.createElement("style");
  menuEnabled: boolean = false;
  showcaseButton: HTMLElement = this.getShowcase();
  logoImg: HTMLElement = this.getLogo();
  menuButton: HTMLElement = this.getMenu();
  blogButton: HTMLElement = this.getBlog();
  aboutUsButton: HTMLElement = this.getAboutUs();
  contactButton: HTMLElement = this.getContact();
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.cssDiv.rel = "stylesheet";
    this.cssDiv.href = this.cssURL.toString();
    //this.styleDiv.textContent = headerStyle
    this.rootDiv = document.createElement("DIV") as HTMLDivElement;
    this.rootDiv.classList.add("NavBar");
    const elementList: Array<Element> = [
      //this.getLogo(),
      //this.getMenu(),
      //this.getShowcase(),
      //this.getBlog(),
      //this.getAboutUs(),
      //this.getContact(),
      this.logoImg,
      this.menuButton,
      this.showcaseButton,
      this.blogButton,
      this.aboutUsButton,
      this.contactButton,
    ];
    elementList.forEach((element: Element) => {
      this.rootDiv.appendChild(element);
    });
    this.shadowRoot?.append(this.cssDiv);
    this.shadowRoot?.append(this.rootDiv);
    this.ensureTranslations();
    anchorupdator(this.rootDiv);
  }
  static init() {
    if (!window.customElements.get("nav-bar")) {
      window.customElements.define("nav-bar", NavBar);
    }
  }
  getLogo() {
    let seal: HTMLImageElement = document.createElement(
      "IMG"
    ) as HTMLImageElement;
    let link: HTMLAnchorElement = document.createElement(
      "A"
    ) as HTMLAnchorElement;
    link.href = new URL("/", this.urlOrigin).toString();
    const sealURL = new URL("../../img/seal.png", import.meta.url);
    seal.alt = "STATE OF DERRENOMO";
    seal.id = "Icon";
    seal.src = sealURL.toString();
    //seal.src = sealImg
    link.appendChild(seal);
    return link;
  }
  getMenu() {
    let menuButton = document.createElement("P");
    menuButton.id = "Menu";
    menuButton.innerText = "Menu";
    menuButton.addEventListener("click", () => {
      this.toggle();
    });
    return menuButton;
  }
  getShowcase() {
    let showcaseButton = document.createElement("P");
    let link: HTMLAnchorElement = document.createElement(
      "A"
    ) as HTMLAnchorElement;
    link.href = new URL(
      "../../pages/showcase.html",
      import.meta.url
    ).toString();
    //link.href = showcaseURL
    link.setAttribute("data-i18n-key", "navbar_showcase");
    link.innerText = "Showcase";
    showcaseButton.classList.add("Link");
    showcaseButton.appendChild(link);
    return showcaseButton;
  }
  getBlog() {
    let blogButton = document.createElement("P");
    let link: HTMLAnchorElement = document.createElement(
      "A"
    ) as HTMLAnchorElement;
    link.href = new URL("/", this.urlOrigin).toString();
    link.setAttribute("data-i18n-key", "navbar_blog");
    link.innerText = "Blog";
    blogButton.classList.add("Link");
    blogButton.appendChild(link);
    return blogButton;
  }
  getAboutUs() {
    let aboutUsButton = document.createElement("P");
    let link: HTMLAnchorElement = document.createElement(
      "A"
    ) as HTMLAnchorElement;
    link.href = new URL("../../pages/aboutus.html", import.meta.url).toString();
    //link.href = aboutUsURL
    link.setAttribute("data-i18n-key", "navbar_aboutus");
    link.innerText = "About Us";
    aboutUsButton.classList.add("Link");
    aboutUsButton.appendChild(link);
    return aboutUsButton;
  }
  getContact() {
    let contactButton = document.createElement("P");
    let link: HTMLAnchorElement = document.createElement(
      "A"
    ) as HTMLAnchorElement;
    link.href = new URL("../../pages/contact.html", import.meta.url).toString();
    //link.href = contactURL
    link.setAttribute("data-i18n-key", "navbar_contact");
    link.innerText = "Contact";
    contactButton.classList.add("Link");
    contactButton.appendChild(link);
    return contactButton;
  }
  enable() {
    const menu = this.rootDiv.querySelector(".NavBar #Menu");
    if (!menu) {
      console.debug("Failed to get menu button (how?)");
      return;
    }
    const items: NodeListOf<Element> | null =
      this.rootDiv.querySelectorAll(".NavBar .Link");
    if (!items) {
      console.debug("Failed to get links (how?)");
      return;
    }
    items.forEach((e: Element) => {
      console.log(e);
      e.classList.add("enabled");
    });
    menu.classList.add("enabled");
    menu.parentElement!.classList.add("enabled");
    this.menuEnabled = true;
    document.body.style.overflow = "hidden";
  }
  disable() {
    const menu = this.rootDiv.querySelector(".NavBar #Menu");
    if (!menu) {
      console.debug("Failed to get menu button (how?)");
      return;
    }
    const items: NodeListOf<Element> | null =
      this.rootDiv.querySelectorAll(".NavBar .Link");
    if (!items) {
      console.debug("Failed to get links (how?)");
      return;
    }
    items.forEach((e) => {
      e.classList.remove("enabled");
    });
    menu.classList.remove("enabled");
    menu.parentElement!.classList.remove("enabled");
    this.menuEnabled = false;
    document.body.style.overflow = "initial";
  }
  toggle() {
    if (!this.menuEnabled) {
      this.enable();
    } else {
      this.disable();
    }
  }
  async ensureTranslations() {
    window.addEventListener("load", async () => {
      new i18nEngine(
        await i18nEngine.getTranslationFile("navbar.json"),
        this.rootDiv
      ).init();
    });
  }
}
