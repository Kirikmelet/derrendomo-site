import i18nEngine from "../i18n/internationalize"

export interface ContentItem {
  image: URL,
  id: string,
  translationEngine?: i18nEngine
}
export interface ContentSwitcherMeta {
  bgDivRootId: string,
  contentBodyRootId: string,
  bgCSSVar: string,
}

export class ContentSwitcher {
  private items: Array<ContentItem>
  private meta: ContentSwitcherMeta
  private itemIndex: number = 0
  public constructor(meta: ContentSwitcherMeta, items: Array<ContentItem>) {
    this.items = items
    this.meta = meta
  }
  public getItems(): Array<ContentItem> { return this.items; }
  public getItem(id: string): ContentItem { return this.items.find((item: ContentItem) => { return item.id === id }) ?? {} as ContentItem }
  public getItemIndex(id: string): number { return this.items.findIndex((item: ContentItem) => { return item.id === id }) }
  public setItem(id: string, newItem: ContentItem): void {
    let index = this.getItemIndex(id)
    if (index < 0) { return; }
    this.items[index] = newItem
  }
  public appendItem(newItem: ContentItem): void {
    this.items.push(newItem)
  }
  public removeItem(id: string): void {
    this.items = this.items.filter((item: ContentItem) => { item.id !== id })
  }
  public getIndex(): number {
    return this.itemIndex
  }
  public setIndex(newIndex: number): void {
    this.itemIndex = (newIndex >= this.items.length) ? this.itemIndex : newIndex
  }
  public shiftIndex(mathShit: number): void {
    //this.itemIndex += (this.itemIndex + mathShit < 0 || this.itemIndex + mathShit >= this.items.length) ? 0 : mathShit
    if (this.itemIndex + mathShit < 0) {
      this.itemIndex = this.items.length - 1
    } else if (this.itemIndex + mathShit >= this.items.length) {
      this.itemIndex = 0
    } else {
      this.itemIndex += mathShit
    }
  }
  public update(): void {
    // Get assets
    const index = this.getIndex()
    if (!this.items[index]) {
      return;
    }
    const bgImage = this.items[index].image.toString()
    const id = this.items[index].id
    const translationEngine: i18nEngine | undefined = this.items[index].translationEngine
    // Get Elements first ofc
    let bgDiv: HTMLDivElement | null = document.querySelector(`#${this.meta.bgDivRootId}`)
    let contentRoot: HTMLDivElement | null = document.querySelector(`#${this.meta.contentBodyRootId}`)
    let placardDiv: HTMLTemplateElement | null = document.querySelector(`template#${id}_placard`)
    let contentDiv: HTMLTemplateElement | null = document.querySelector(`template#${id}`)
    if (!bgDiv || !placardDiv || !contentDiv || !contentRoot) {
      console.error("Failed to get ContentSwitcher divs!")
      console.debug(bgDiv, placardDiv, contentDiv, contentRoot)
      return;
    }
    // Clear enabled item
    document.querySelectorAll("._ContentItem").forEach((el: Node) => {
      const element = (el as HTMLElement)
      element.parentElement?.removeChild(element)
    })
    // Populate with new item
    let placardContent = placardDiv.content.cloneNode(true)
    let contentContent = contentDiv.content.cloneNode(true)
    placardContent.childNodes.forEach
    bgDiv.style.setProperty(this.meta.bgCSSVar, `url("${bgImage}")`)
    bgDiv.appendChild(placardContent)
    contentRoot.appendChild(contentContent)
    const newURL = new URL(location.href)
    newURL.searchParams.set("searchIndex", index.toString())
    history.pushState({ path: newURL.toString() }, "", newURL.toString())
    if (translationEngine) {
      translationEngine.init(true)
    }
  }
}
