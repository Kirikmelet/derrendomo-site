export default (root: HTMLElement = document.documentElement) => {
  const anchors: NodeListOf<HTMLAnchorElement> = root.querySelectorAll("a");
  anchors.forEach((anchor: HTMLAnchorElement | null) => {
    if (!anchor) {
      return;
    }
    const locale = new URLSearchParams(document.location.search).get("locale");
    anchor.addEventListener("click", (event: MouseEvent) => {
      event.preventDefault();
      const oldUrl = anchor.href;
      location.href =
        oldUrl + (locale !== "null" && locale ? `?locale=${locale}` : "");
    });
  });
};
