export default class SlideShow {
  div: HTMLDivElement | null;
  divAfter: CSSStyleDeclaration;
  list: any;
  totalImages: number;
  isBeforeEl: boolean = false;
  index: number = 0;
  constructor(path: URL | String, total_images: number, generator: Function) {
    this.div = document.querySelector(".SlideShow");
    if (!this.div) {
      throw TypeError;
    }
    this.divAfter = window.getComputedStyle(this.div, ":after");
    this.list = generator(total_images, path);
    this.totalImages = total_images;
    this.isBeforeEl = false;
  }
  start() {
    setInterval(() => {
      this.cycle();
      this.update();
    }, 5000);
  }
  update() {
    if (!this.div) {
      return;
    }
    if (this.isBeforeEl) {
      this.div.style.setProperty("--1st-bg", this.list[this.index]);
      this.div.style.setProperty("--2nd-opacity", "0");
    } else {
      this.div.style.setProperty("--2nd-bg", this.list[this.index]);
      this.div.style.setProperty("--2nd-opacity", "1");
    }
    this.isBeforeEl = !this.isBeforeEl;
  }
  cycle() {
    this.index = this.index >= this.totalImages - 1 ? 0 : this.index + 1;
  }
}
