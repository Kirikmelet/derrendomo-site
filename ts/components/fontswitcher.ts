import i18nEngine from "../i18n/internationalize.js";

interface languageData {
  [lang: string]: string;
}


export default (
  query: Array<NodeListOf<Element>>,
  data: languageData
) => {
  if (!query) {
    return;
  }
  const lang = i18nEngine.getLocale();
  console.log(lang, data[lang]);
  if (data[lang]) {
    query.forEach((el: NodeListOf<Element>) => {
      el.forEach((el: Element) => {
        if (!el) {
          return;
        }
        (el as HTMLElement).style.fontFamily = data[lang];
      });
    });
  }
};
