import i18nEngine from "../i18n/internationalize.js";
export default (query, data) => {
    if (!query) {
        return;
    }
    const lang = i18nEngine.getLocale();
    console.log(lang, data[lang]);
    if (data[lang]) {
        query.forEach((el) => {
            el.forEach((el) => {
                if (!el) {
                    return;
                }
                el.style.fontFamily = data[lang];
            });
        });
    }
};
