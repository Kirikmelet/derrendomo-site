export default (root = document.documentElement) => {
    const anchors = root.querySelectorAll("a");
    anchors.forEach((anchor) => {
        if (!anchor) {
            return;
        }
        const locale = new URLSearchParams(document.location.search).get("locale");
        anchor.addEventListener("click", (event) => {
            event.preventDefault();
            const oldUrl = anchor.href;
            location.href =
                oldUrl + (locale !== "null" && locale ? `?locale=${locale}` : "");
        });
    });
};
