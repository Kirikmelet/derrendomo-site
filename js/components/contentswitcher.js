export class ContentSwitcher {
    constructor(meta, items) {
        this.itemIndex = 0;
        this.items = items;
        this.meta = meta;
    }
    getItems() { return this.items; }
    getItem(id) { return this.items.find((item) => { return item.id === id; }) ?? {}; }
    getItemIndex(id) { return this.items.findIndex((item) => { return item.id === id; }); }
    setItem(id, newItem) {
        let index = this.getItemIndex(id);
        if (index < 0) {
            return;
        }
        this.items[index] = newItem;
    }
    appendItem(newItem) {
        this.items.push(newItem);
    }
    removeItem(id) {
        this.items = this.items.filter((item) => { item.id !== id; });
    }
    getIndex() {
        return this.itemIndex;
    }
    setIndex(newIndex) {
        this.itemIndex = (newIndex >= this.items.length) ? this.itemIndex : newIndex;
    }
    shiftIndex(mathShit) {
        //this.itemIndex += (this.itemIndex + mathShit < 0 || this.itemIndex + mathShit >= this.items.length) ? 0 : mathShit
        if (this.itemIndex + mathShit < 0) {
            this.itemIndex = this.items.length - 1;
        }
        else if (this.itemIndex + mathShit >= this.items.length) {
            this.itemIndex = 0;
        }
        else {
            this.itemIndex += mathShit;
        }
    }
    update() {
        // Get assets
        const index = this.getIndex();
        if (!this.items[index]) {
            return;
        }
        const bgImage = this.items[index].image.toString();
        const id = this.items[index].id;
        const translationEngine = this.items[index].translationEngine;
        // Get Elements first ofc
        let bgDiv = document.querySelector(`#${this.meta.bgDivRootId}`);
        let contentRoot = document.querySelector(`#${this.meta.contentBodyRootId}`);
        let placardDiv = document.querySelector(`template#${id}_placard`);
        let contentDiv = document.querySelector(`template#${id}`);
        if (!bgDiv || !placardDiv || !contentDiv || !contentRoot) {
            console.error("Failed to get ContentSwitcher divs!");
            console.debug(bgDiv, placardDiv, contentDiv, contentRoot);
            return;
        }
        // Clear enabled item
        document.querySelectorAll("._ContentItem").forEach((el) => {
            const element = el;
            element.parentElement?.removeChild(element);
        });
        // Populate with new item
        let placardContent = placardDiv.content.cloneNode(true);
        let contentContent = contentDiv.content.cloneNode(true);
        placardContent.childNodes.forEach;
        bgDiv.style.setProperty(this.meta.bgCSSVar, `url("${bgImage}")`);
        bgDiv.appendChild(placardContent);
        contentRoot.appendChild(contentContent);
        const newURL = new URL(location.href);
        newURL.searchParams.set("searchIndex", index.toString());
        history.pushState({ path: newURL.toString() }, "", newURL.toString());
        if (translationEngine) {
            translationEngine.init(true);
        }
    }
}
