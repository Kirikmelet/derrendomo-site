import i18nEngine from "../i18n/internationalize.js";
import AnchorUpdator from "../components/anchorupdator.js";
const main = async () => {
    window.addEventListener("load", async () => {
        new i18nEngine({}).init();
        AnchorUpdator();
    });
};
new Promise(() => {
    main();
});
