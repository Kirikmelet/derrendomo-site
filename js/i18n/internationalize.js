/* DATA NOTATION
 * {lang}: {
 *  {key}: {data}
 * }
 *
 */
export default class i18nEngine {
    constructor(data, root) {
        this.hasRan = false;
        this.data = data;
        this.lang = i18nEngine.getLocale();
        this.root = root ?? document.documentElement;
    }
    init(forceRun) {
        if ((this.hasRan && !forceRun) == true) {
            console.debug("Has already ran, returning");
            return;
        }
        if (Object.keys(this.data).length == 0) {
            console.error("Failed to get translation JSON file");
            return;
        }
        // Update html lang
        if (!this.data[this.lang]) {
            this.lang = "en"; // Fuck it
        }
        document.documentElement.lang = this.lang;
        this.root
            .querySelectorAll("[data-i18n-key]")
            .forEach(this.localize.bind(this));
        this.hasRan = true;
    }
    static getLocale() {
        const localeQuery = new URL(document.location.toString()).searchParams.get("locale");
        const lang = localeQuery ? localeQuery : navigator.language;
        return lang.replace(/(-.*)/, "");
    }
    localize(element) {
        const key = element.getAttribute("data-i18n-key");
        if (!key) {
            return;
        }
        let translation = "";
        try {
            let check_data = this.data[this.lang][key];
            translation = check_data ? check_data : element.innerHTML;
        }
        catch (e) {
            let check_data = this.data["en"][key];
            translation = check_data ? check_data : element.innerHTML;
        }
        finally {
            element.innerHTML = this.sanitize(translation);
        }
    }
    sanitize(str) {
        const reg = new RegExp(/{([^}]+)}/, "g");
        let text = null;
        let results = [];
        while ((text = reg.exec(str))) {
            results.push(text[1]);
        }
        results.forEach((e) => {
            str = str.replace(`{${e}}`, this.data[this.lang][e]);
        });
        return str;
    }
    static async getTranslationFile(file) {
        const rootURL = new URL("../../translation_data/", import.meta.url);
        let path = new URL(file, rootURL);
        console.debug(import.meta.url, rootURL, path);
        try {
            return await (await fetch(path)).json();
        }
        catch {
            return {};
        }
    }
}
